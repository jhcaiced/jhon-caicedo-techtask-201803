<?php
require_once __DIR__ . '/../vendor/autoload.php';

use RecipeManager\Model\RecipeList;
use RecipeManager\Util\Date;

$app = new Silex\Application();

$app->get('/', function () use ($app) {
    return $app->json([
        'data' => [
            'status' => 'ok'
        ]
    ]);
});

$app->get('/lunch/{query}', function ($query) use ($app) {
    $recipes = new RecipeList();
    return $app->json([
        'data' => [
            'status' => 'ok',
            'query' => $query,
            'recipes' => $query === 'all' ? $recipes->queryAll() : $recipes->queryRecipesByDate(new Date($query))
        ]
    ]);
})->value('query', (Date::today())->toIsoDate());

$app->run();
