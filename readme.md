# Sample Silex App + Docker Setup + unit tests

## Starting development environment
A basic docker-compose.yml and Dockerfile are provided for development, to start the environment use:

`docker-compose up`

## Running tests
First, update composer dependencies locally, then run the tests using

`TEST_API_URL=http://localhost:8080 ./vendor/bin/phpunit`

## Endpoints

The silex app will answer to queries like

`GET /lunch`  Will show the recipes available using today's date

`GET /lunch/all` will show all the recipes available

`GET /lunch/<YYYY-MM-DD>` will show all the recipes on date YYYY-MM-DD

## Notes
* As this is only an exercise, input validation was not added
* Developed the app using PHP-7.1
