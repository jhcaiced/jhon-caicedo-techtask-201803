<?php

namespace UnitTest;

use PHPUnit\Framework\TestCase;

use RecipeManager\Util\Date;

final class DateTest extends TestCase
{
    public function testToday()
    {
        $this->assertEquals(
            (Date::today())->format('Y-m-d'),
            (new \DateTime())->format('Y-m-d')
        );
    }

    public function testIsoDate()
    {
        $this->assertEquals(
            (Date::today())->toIsoDate(),
            (new \DateTime())->format('Y-m-d')
        );
    }

    public function testDifferenceInDays()
    {
        $date = new Date('2018-03-10');
        $this->assertEquals($date->differenceInDays(new Date('2018-03-12')), -2);
        $this->assertEquals($date->differenceInDays(new Date('2018-03-08')), 2);
        $this->assertEquals($date->differenceInDays(new Date('2018-03-10')), 0);
    }

    public function testLessThan()
    {
        $this->assertTrue((new Date('2018-03-09'))->lessThan(new Date('2018-03-10')));
        $this->assertFalse((new Date('2018-03-10'))->lessThan(new Date('2018-03-10')));
    }

    public function testGreaterThan()
    {
        $this->assertTrue((new Date('2018-03-10'))->greaterThan(new Date('2018-03-09')));
        $this->assertTrue((new Date('2018-03-10'))->greaterThan(new Date('2018-03-10')));
    }
}
