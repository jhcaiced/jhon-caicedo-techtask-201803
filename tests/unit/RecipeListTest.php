<?php

namespace UnitTest;

use PHPUnit\Framework\TestCase;

use RecipeManager\Model\RecipeList;
use RecipeManager\Util\Date;

final class RecipeListTest extends TestCase
{
    public function testRecipesByDate()
    {
        $response = (new RecipeList())->queryRecipesByDate(new Date('2018-03-01'));
        $this->assertEquals(count($response), 3);

        $response = (new RecipeList())->queryRecipesByDate(new Date('2018-03-13'));
        $this->assertEquals(count($response), 2);

        $response = (new RecipeList())->queryRecipesByDate(new Date('2018-03-20'));
        $this->assertEquals(count($response), 1);
    }

    public function testRecipesSorting()
    {
        $response = (new RecipeList())->queryRecipesByDate(new Date('2018-03-13'));
        $this->assertEquals($response[0]['title'], 'Hotdog');
        $this->assertEquals($response[1]['title'], 'Ham and Cheese Toastie');
    }
}
