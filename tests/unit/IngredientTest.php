<?php


namespace UnitTest;

use PHPUnit\Framework\TestCase;

use RecipeManager\Model\Ingredient;
use RecipeManager\Util\Date;

final class IngredientTest extends TestCase
{
    public function testExpired()
    {
        $ingredient = new Ingredient('Test', new Date('2018-03-10'), new Date('2018-03-05'));
        $this->assertTrue($ingredient->hasExpired(new Date('2018-03-12')));
        $this->assertFalse($ingredient->hasExpired(new Date('2018-03-10')));
        $this->assertFalse($ingredient->hasExpired(new Date('2018-03-09')));
    }
}
