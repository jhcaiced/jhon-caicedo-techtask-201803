<?php


namespace UnitTest;

use PHPUnit\Framework\TestCase;

use RecipeManager\Model\Recipe;
use RecipeManager\Model\Ingredient;
use RecipeManager\Util\Date;

final class RecipeTest extends TestCase
{
    public function testExpired()
    {
        $recipe = new Recipe('Test Recipe');

        $this->assertEquals($recipe->title(), 'Test Recipe');
        $this->assertEquals($recipe->ingredientsCount(), 0);
        $recipe->add(new Ingredient('Ingredient #1', new Date('2018-03-10'), new Date('2018-03-05')));
        $recipe->add(new Ingredient('Ingredient #1', new Date('2018-03-13'), new Date('2018-03-08')));
        $this->assertEquals($recipe->ingredientsCount(), 2);
        $this->assertFalse($recipe->hasExpiredIngredients(new Date('2018-03-09')));
        $this->assertFalse($recipe->hasExpiredIngredients(new Date('2018-03-10')));
        $this->assertTrue($recipe->hasExpiredIngredients(new Date('2018-03-11')));

        $this->assertEquals($recipe->getFreshScore(new Date('2018-03-08')), -3);
        $this->assertEquals($recipe->getFreshScore(new Date('2018-03-05')), 0);
        $this->assertEquals($recipe->getFreshScore(new Date('2018-03-10')), -7);
    }
}
