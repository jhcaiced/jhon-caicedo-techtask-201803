<?php

namespace ApiTest;

final class QueryAllTest extends ApiTestCase
{
    public function testQueryAllRecipes()
    {
        $response = $this->http->get($this->baseUrl . 'lunch/all');
        $this->assertEquals(200, $response->getStatusCode());
        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['recipes']));
        $this->assertTrue(count($body['data']['recipes']) === 3);
    }
}
