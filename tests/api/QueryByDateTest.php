<?php

namespace ApiTest;

final class QueryByDateTest extends ApiTestCase
{
    public function queryLunch($date)
    {
        return $this->http->get($this->baseUrl . 'lunch/' . $date);
    }

    public function testQueryRecipesByDate()
    {
        $response = $this->queryLunch('2018-03-01');
        $this->assertEquals(200, $response->getStatusCode());
        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['recipes']));
        $this->assertTrue(count($body['data']['recipes']) === 3);

        $response = $this->queryLunch('2018-03-13');
        $this->assertEquals(200, $response->getStatusCode());
        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['recipes']));
        $this->assertTrue(count($body['data']['recipes']) === 2);

        $response = $this->queryLunch('2018-03-20');
        $this->assertEquals(200, $response->getStatusCode());
        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['recipes']));
        $this->assertTrue(count($body['data']['recipes']) === 1);
    }

    public function testQueryRecipesSorting()
    {
        $response = $this->queryLunch('2018-03-13');
        $this->assertEquals(200, $response->getStatusCode());
        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['recipes']));
        $this->assertTrue($body['data']['recipes'][0]['title'] === 'Hotdog');
        $this->assertTrue($body['data']['recipes'][1]['title'] === 'Ham and Cheese Toastie');
    }
}
