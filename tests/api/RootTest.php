<?php

namespace ApiTest;

final class RootTest extends ApiTestCase
{
    public function testApiRoot()
    {
        $response = $this->http->get($this->baseUrl);
        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()['Content-Type'][0];
        $this->assertEquals('application/json', $contentType);

        $body = $this->jsonResponse($response);
        $this->assertTrue(isset($body['data']['status']));
        $this->assertTrue($body['data']['status'] === 'ok');
    }
}
