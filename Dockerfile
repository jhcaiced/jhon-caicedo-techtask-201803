FROM php:fpm
LABEL maintainer="Jhon H. Caicedo"
LABEL e-mail="jhcaiced@inticol.com"

RUN apt-get -y update && apt-get -y install curl git unzip

WORKDIR /opt

# Install composer to handle dependencies
RUN curl --silent --show-error https://getcomposer.org/installer > /tmp/composer-setup.php && \
  php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --snapshot

# Cache composer dependencies between builds
ADD composer.json /tmp/composer.json
ADD composer.lock /tmp/composer.lock
RUN cd /tmp && composer install --no-scripts --no-autoloader --prefer-source --no-interaction
RUN cp -a /tmp/vendor /opt

COPY . /opt

RUN composer install && composer dump-autoload --optimize

