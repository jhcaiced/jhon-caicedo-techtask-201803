<?php

namespace RecipeManager\Util;

class Date extends \DateTime
{
    public function differenceInDays(Date $date)
    {
        return $this->diff($date)->format('%R%a') * -1;
    }

    public static function today()
    {
        return new self((new \DateTime())->format('Y-M-d'));
    }

    public function toIsoDate()
    {
        return $this->format('Y-m-d');
    }

    public function lessThan(Date $date)
    {
        return $this->differenceInDays($date) < 0;
    }

    public function greaterThan(Date $date)
    {
        return $this->differenceInDays($date) >= 0;
    }
}
