<?php

namespace RecipeManager\Model;

use RecipeManager\Util\Date;
use RecipeManager\Model\Ingredient;
use RecipeManager\Model\Recipe;

class RecipeList
{
    private $recipes;

    public function __construct()
    {
        $recipesFile = dirname(dirname(dirname(__FILE__))) . '/files/data/recipes.json';
        $ingredientsFile = dirname(dirname(dirname(__FILE__))) . '/files/data/ingredients.json';

        $data = json_decode(file_get_contents($ingredientsFile), true);
        $this->ingredients = [];
        foreach ($data['ingredients'] as $ingredient) {
            $this->ingredients[$ingredient['title']] = $ingredient;
        }

        $data = json_decode(file_get_contents($recipesFile), true);
        $this->recipes = [];
        foreach ($data['recipes'] as $recipe) {
            $detailedRecipe = new Recipe($recipe['title']);

            $foundAllIngredients = true;
            foreach ($recipe['ingredients'] as $ingredientName) {
                if (!isset($this->ingredients[$ingredientName])) {
                    $foundAllIngredients = false;
                    break;
                }
                $ingredient = $this->ingredients[$ingredientName];
                $detailedRecipe->add(new Ingredient(
                    $ingredient['title'],
                    new Date($ingredient['use-by']),
                    new Date($ingredient['best-before'])
                ));
            }
            if ($foundAllIngredients) {
                $this->recipes[] = $detailedRecipe;
            }
        }
    }

    public function queryAll()
    {
        return array_map(function ($recipe) {
            return $recipe->asArray();
        }, $this->recipes);
    }

    public function queryRecipesByDate(Date $date)
    {
        // Remove recipes with expired ingredients
        $recipes = $this->filterByExpirationDate($this->recipes, $date);
        // Sort recipes by expiration date
        $sorted = $this->sortByBestBeforeDate($recipes, $date);
        return array_map(function ($recipe) {
            return $recipe->asArray();
        }, $sorted);
    }

    protected function filterByExpirationDate($recipes, Date $date)
    {
        return array_filter($recipes, function ($recipe) use ($date) {
            return !$recipe->hasExpiredIngredients($date);
        });
    }

    protected function sortByBestBeforeDate($recipes, Date $date)
    {
        usort($recipes, function ($recipeA, $recipeB) use ($date) {
            return $recipeA->getFreshScore($date) < $recipeB->getFreshScore($date) ? 1 : -1;
        });
        return $recipes;
    }
}
