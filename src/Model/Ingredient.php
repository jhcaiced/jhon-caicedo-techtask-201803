<?php

namespace RecipeManager\Model;

use RecipeManager\Util\Date;

class Ingredient
{
    private $title;
    private $useBy;
    private $bestBefore;

    public function __construct($title, Date $useBy, Date $bestBefore)
    {
        $this->title = $title;
        $this->useBy = $useBy;
        $this->bestBefore = $bestBefore;
    }

    public function title()
    {
        return $this->title;
    }

    public function useBy()
    {
        return $this->useBy;
    }

    public function bestBefore()
    {
        return $this->bestBefore;
    }

    public function hasExpired(Date $date)
    {
        return $this->useBy->lessThan($date);
    }
}
