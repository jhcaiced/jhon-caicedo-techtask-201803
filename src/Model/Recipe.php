<?php

namespace RecipeManager\Model;

use RecipeManager\Util\Date;

class Recipe
{
    private $title;
    private $ingredients;

    public function __construct($title)
    {
        $this->title = $title;
        $this->ingredients = [];
    }

    public function title()
    {
        return $this->title;
    }

    public function ingredients()
    {
        return $this->ingredients;
    }

    public function ingredientsCount()
    {
        return count($this->ingredients);
    }

    public function asArray()
    {
        return [
            'title' => $this->title
        ];
    }

    public function add(Ingredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
    }

    public function hasExpiredIngredients(Date $date)
    {
        foreach ($this->ingredients as $ingredient) {
            if ($ingredient->hasExpired($date)) {
                return true;
            }
        }
        return false;
    }

    public function getFreshScore(Date $date)
    {
        $score = 0;
        foreach ($this->ingredients as $ingredient) {
            $days = $date->differenceInDays($ingredient->bestBefore());
            if ($days > 0) {
                $score -= $days;
            }
        }
        return $score;
    }
}
